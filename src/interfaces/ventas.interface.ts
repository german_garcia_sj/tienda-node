import { Document } from 'mongoose'
import { ObjectId } from 'mongodb';

export default interface IVenta extends Document {
  _id: string;
  forma_de_pago: string;
  precio_total: number;
  estado: string;
  persona_id: Array<String>;
  productos: Array<String>;
  createdAt: Date;
  updatedAt: Date;
};
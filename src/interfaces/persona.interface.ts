import { Document } from 'mongoose'

export default interface IPersona extends Document {
  _id: string;
  nombreCompleto: string;
  email: string;
  psw: string;
  telefono: string;
  rol: string;
  estado:boolean;
  createdAt: Date;
  updatedAt: Date;
};
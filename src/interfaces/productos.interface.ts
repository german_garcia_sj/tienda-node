import { Document } from 'mongoose'
import { ObjectId } from 'mongodb';

export default interface IProduct extends Document {
  _id: string;
  nombre: string;
  precio: number;
  stock: number;
  estado: string;
  venta: Array<String>;
  createdAt: Date;
  updatedAt: Date;
  
};
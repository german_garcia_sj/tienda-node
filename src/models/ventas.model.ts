import { model, Schema } from 'mongoose';
import IVenta from '../interfaces/ventas.interface';


const ProductoSchema = new Schema({
    forma_de_pago: {
        type: String,
        required: [true, 'El nombre es obligatorio'],
        enum: ['Contado', 'T_Credito']
    },
    precio_total: {
        type: Number
    },
    estado: {
        type: String,
        required: [true, 'El estado es obligatorio. Valores posibles: nuevo/usado'],
        enum: ['Aprobada', 'Anulada']
    },
    persona_id:{
        type: Schema.Types.ObjectId,
        ref: 'Persona',
        required: true
    },
    productos: [{
        type: Schema.Types.ObjectId,
        ref: 'Producto',
        required: true
      }],
}, {
    timestamps: { createdAt: true, updatedAt: true }
})

export default model<IVenta>('Venta', ProductoSchema);
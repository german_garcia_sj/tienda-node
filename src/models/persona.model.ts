import { model, Schema } from 'mongoose';
import IPersona from '../interfaces/persona.interface';

const PersonaSchema = new Schema({
    nombreCompleto: {
        type: String,
        required: [true, 'El nombre es obligatorio']
    },
    email: {
        type: String,
        required: [true, 'El email es obligatorio y único'],
        unique: true 
    },
    psw: {
        type: String,
        required: [true, 'El psw es obligatorio']
    },
    telefono: {
        type: String,
        required: false
    },
    rol: {
        type: String,
        required: [true, 'El Rol es obligatorio. Valores posibles: admin,cliente'],
        enum: ['admin', 'cliente']
    }, 
    estado:{
        type: Boolean,
        default: true
    }
}, {
    timestamps: { createdAt: true, updatedAt: true }
})

export default model<IPersona>('Persona', PersonaSchema);
import { Router } from 'express';
import * as personaController from '../controllers/persona.controller';
import { body, param, query } from 'express-validator';
import { validPersonaEmail, validPersonaId } from '../helper/db-validator';
import { validateFields } from '../middlewares/validate-fields';

const router = Router();
/*
router.post('/login', personaController.login);
router.post('/', [
  body('email').custom(validPersonaEmail),
  validateFields
],personaController.store);*/

//POST c/validaciones
router.post('/',[
  body('nombreCompleto', 'El nombre debe tener como minimo 2 caracteres').isString().isLength({ min: 2, max: 20 }),
  body('email', 'El nombre debe tener como minimo 2 caracteres').isString().isLength({ min: 2, max: 20 }),
  body('psw', 'El nombre debe tener como minimo 2 caracteres').isString().isLength({ min: 2, max: 20 }),
  body('telefono', 'El nombre debe tener como minimo 2 caracteres').optional().isString(),
  body('rol', 'El nombre debe tener como minimo 2 caracteres').isString().isIn(['admin', 'cliente']),
  body('estado', 'El nombre debe tener como minimo 2 caracteres').optional().isBoolean(),
  body('email').custom(validPersonaEmail),
  validateFields
], personaController.create);

//GET c/validaciones para filtros
router.get('/', [
    query('nombreCompleto', 'El nombre debe tener como minimo 2 caracteres').optional().isString().isLength({ min: 2, max: 20 }),
    query('email', 'El nombre debe tener como minimo 2 caracteres').optional().isString().isLength({ min: 2, max: 20 }),
    query('psw', 'El nombre debe tener como minimo 2 caracteres').optional().isString().isLength({ min: 2, max: 20 }),
    query('telefono', 'El nombre debe tener como minimo 2 caracteres').optional().isString().isLength({ min: 2, max: 20 }),
    query('rol', 'El nombre debe tener como minimo 2 caracteres').optional().isString().isIn(['admin', 'cliente']),
    query('estado', 'El nombre debe tener como minimo 2 caracteres').optional().isBoolean(),
    validateFields
],personaController.index);

//GET c/validación ID formato Mongo
router.get('/:id', [
  param('id', 'El id debe cumplir con el formato de id de mongo').isMongoId(),
  param('id').custom(validPersonaId),
  validateFields
],personaController.show);

//PUT c/validaciones
router.put('/:id', [
  body('nombreCompleto', 'El nombre debe tener como minimo 2 caracteres').optional().isString().isLength({ min: 2, max: 20 }),
  body('psw', 'El nombre debe tener como minimo 2 caracteres').optional().isString().isLength({ min: 2, max: 20 }),
  body('telefono', 'El nombre debe tener como minimo 2 caracteres').optional().isString().isLength({ min: 2, max: 20 }),
  body('rol', 'Roles posible cliente o admin').optional().isString().isIn(['admin', 'cliente']),
  param('id').custom(validPersonaId),
  validateFields
],personaController.update);

//DELETE c/validación ID formato Mongo
router.delete('/:id', [
  param('id', 'El id debe cumplir con el formato de id de mongo').isMongoId(),
  param('id').custom(validPersonaId),
  validateFields
],personaController.destroy);

export default router;








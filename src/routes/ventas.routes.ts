import { Router } from 'express';
import * as ventasController from '../controllers/ventas.controller';
import { body, param, query } from 'express-validator';
import { validVentaId,validPersonaId,validProductoId } from '../helper/db-validator';
import { validateFields } from '../middlewares/validate-fields';

const router = Router();
//POST c/validaciones
router.post('/',[
    body('forma_de_pago', 'Seleccionar Contado o T_Credito').isString().isIn(['Contado', 'T_Credito']),
    body('persona_id', 'El id debe cumplir con el formato de id de mongo').isMongoId(),
    body('persona_id').custom(validPersonaId),
    validateFields
], ventasController.create);

//POST Agregar producto a la venta
router.post('/:ventaId/productos/:productoId',[
    param('Producto', 'El id debe cumplir con el formato de id de mongo').isMongoId(),
    param('id', 'El id debe cumplir con el formato de id de mongo').isMongoId(),
],ventasController.agregarPoducto);

//GET c/validaciones para filtros
router.get('/',[
    query('forma_de_pago', 'Seleccionar Contado o T_Credito').optional().isString().isIn(['Contado', 'T_Credito']),
    query('precio_total', 'El minimo es cero (0)').optional().isInt({ min: 0 }),
    query('estado', 'El nombre debe tener como minimo 2 caracteres').optional().isString().isIn(['Aprobada', 'Anulada']),
    query('persona_id', 'El id debe cumplir con el formato de id de mongo').optional().isMongoId(),
    query('persona_id').custom(validPersonaId).optional(),
    query('Producto', 'El id debe cumplir con el formato de id de mongo').optional().isMongoId(),
    query('Producto').custom(validProductoId).optional(),
    query('id', 'El id debe cumplir con el formato de id de mongo').optional().isMongoId(),
    query('id').custom(validVentaId).optional(),
    validateFields
], ventasController.index);

//GET c/validación ID formato Mongo
router.get('/:id',[
    param('id', 'El id debe cumplir con el formato de id de mongo').isMongoId(),
    param('id').custom(validVentaId),
    validateFields
], ventasController.show);

//PUT c/validaciones
router.put('/:id',[
    body('forma_de_pago', 'Seleccionar Contado o T_Credito').optional().isString().isIn(['Contado', 'T_Credito']),
    body('id', 'El id debe cumplir con el formato de id de mongo').optional().isMongoId(),
    param('id').custom(validVentaId),
    validateFields
], ventasController.update);

//DELETE c/validación ID formato Mongo
router.delete('/:id',[
  param('id', 'El id debe cumplir con el formato de id de mongo').isMongoId(),
  param('id').custom(validVentaId),
  validateFields
], ventasController.destroy);

//DELETE Borrar producto de la venta
router.delete('/:ventaId/productos/:productoId',[
    param('Producto', 'El id debe cumplir con el formato de id de mongo').isMongoId(),
    param('id', 'El id debe cumplir con el formato de id de mongo').isMongoId(),
],ventasController.borrarproducto);
export default router;
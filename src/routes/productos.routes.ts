import { Router } from 'express';
import * as productoController from '../controllers/productos.controller';
import { body, param, query } from 'express-validator';
import { validProductoId } from '../helper/db-validator';
import { validateFields } from '../middlewares/validate-fields';


const router = Router();
//POST c/validaciones
router.post('/', [
  body('nombre', 'El nombre es obligatorio').isString(),
  body('precio', 'El precio es obligatorio').isInt({ min: 1, max: 1000000}),
  body('stock', 'El stock es obligatorio').isInt({ min: 0}),
  body('estado', 'El estado es obligatorio').isString().isIn(['nuevo', 'usado']),
  validateFields
], productoController.store);


//GET c/validaciones para filtros
router.get('/', [
  query('nombre', 'El nombre debe tener como minimo 2 caracteres').optional().isString().isLength({ min: 2, max: 20 }),
  query('precio', 'El nombre debe tener como minimo 2 caracteres').optional().isInt({min: 1, max:1000000}),
  query('stock', 'El nombre debe tener como minimo 2 caracteres').optional().isInt({ min: 0}),
  query('estado', 'El nombre debe tener como minimo 2 caracteres').optional().isString().isIn(['nuevo', 'usado']),
  validateFields
],productoController.index);

//GET c/validación ID formato Mongo
router.get('/:id', [
  param('id', 'El id debe cumplir con el formato de id de mongo').isMongoId(),
  param('id').custom(validProductoId),
  validateFields
],productoController.show);

//PUT c/validaciones
router.put('/:id', [
  body('nombre', 'El nombre es obligatorio').optional().isString(),
  body('precio', 'El precio es obligatorio').optional().isInt({ min: 1, max: 1000000}),
  body('stock', 'El stock es obligatorio').optional().isInt({ min: 0}),
  body('estado', 'El estado es obligatorio (nuevo o usado)').optional().isString().isIn(['nuevo', 'usado']),
  param('id').custom(validProductoId),
  validateFields
],productoController.update);

//DELETE c/validación ID formato Mongo
router.delete('/:id',[
  param('id', 'El id debe cumplir con el formato de id de mongo').isMongoId(),
  param('id').custom(validProductoId),
  validateFields
], productoController.destroy);

export default router;
import winston from 'winston'

const levels = {
  error: 0,
  warn: 1,
  info: 2,
  http: 3,
  verbose: 4,
  debug: 5,
  silly: 6
  }
  /*const colors = {
    error: 'red',
    warn: 'yellow',
    info: 'green',
    http: 'magenta',
    debug: 'white',
  }
  winston.addColors(colors)*/

  const level = () => {
    const env = process.env.NODE_ENV || 'development'
    const isDevelopment = env === 'development'
    return isDevelopment ? 'debug' : 'warn'
  }
const format = winston.format.combine(
    winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
    /*winston.format.colorize({ all: true }),*/
    winston.format.printf(
      (info) => `${info.timestamp} - ${info.level}: ${info.message}`,
    ),
  )
  
  const transports = [
    new winston.transports.Console(),
    new winston.transports.File({
      filename: 'logs/error.log',
      level: 'error',
    }),
    new winston.transports.File({ filename: 'logs/all.log' }),
    new winston.transports.Http({
        level: 'warn',
        format: winston.format.json()
      })
  ]
  
  const Logger = winston.createLogger({
    level: 'debug',// level(),
    levels,
    format,
    transports,
  })
  
  export default Logger



/*

const {createLogger, format, transports} = require('winston');
const {combine, timestamp, printf} = format;
const myFormat = printf((info: {level: string, timestamp: string}) => {
    return `${timestamp} ${level}: `;
});

module.exports = createLogger({

    format: combine(
        timestamp(),
        myFormat
    ),

    transports: [
        new transports.File({
            maxsize:5120000, //5mb por archivo
            maxFiles: 5,      //max 5 archivos en la carpeta (los últimos 5)
            filename: `${__dirname}/../logs/log-api.log`
        }),
        new transports.Console({
            level: 'debug'
        })
    ]
}); */
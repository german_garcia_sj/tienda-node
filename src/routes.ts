import { Application } from 'express';

// Import routes
import ProductosRoutes from './routes/productos.routes';
import PersonaRoutes from './routes/persona.routes';
import VentasRoutes from './routes/ventas.routes';
import MateriasRoutes from './routes/materias.routes';
import AlumnosRoutes from './routes/alumnos.routes';

export const registeredRoutes = async (app:Application) => {
  app.use('/api/productos', ProductosRoutes);
  app.use('/api/personas', PersonaRoutes);
  app.use('/api/ventas', VentasRoutes);
  app.use('/api/alumnos', AlumnosRoutes);
  app.use('/api/materias', MateriasRoutes);
};
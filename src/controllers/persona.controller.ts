import { Request, Response } from "express";
import Persona from "../models/persona.model";
import IPersona from "../interfaces/persona.interface";
import Logger from '../utils/loguer'
import app  from '../app';
import bcrypt from 'bcrypt';

// Get all resources
export const index = async (req: Request, res: Response) => {
    // agregar filtros

    try {
        const { ...data } = req.query; 
        let filters = { ...data };//preguntar por esta línea??
        
        if (data.nombreCompleto) { //preguntar por esta validacion ej (data solo)
            filters = { ...filters, nombre: {$regex: data.nombreCompleto, $options: 'i'} }
        }

        let persona = await Persona.find(filters);

        res.json(persona);
        Logger.info('Metodo GET ALL - Status 200');
    } catch (error) {
        res.status(500).send('Algo salió mal');
        Logger.info('Metodo GET ALL - Status 500 - Algo salió mal.');
    }
};

// Get one resource
export const show = async (req: Request, res: Response) => {
    const id = req?.params?.id;
    try {
        let persona = await Persona.findById(id);

        if (!persona){
            res.status(404).send(`No se encontró el producto con id: ${id}`);
            Logger.info('Metodo GET ONE - Status 404 - '+`No se encontró el producto con id: ${id}`);
        }
        else {
            res.json(persona);
            Logger.info('Metodo GET ONE - Status 200');
            Logger.info(persona);
        }
    } catch (error) {
        res.status(500).send('Algo salió mal.');
        Logger.info('Metodo GET ONE - Status 500 - Algo salió mal.');
    }
};

// Create a new resource
export const create = async (req: Request, res: Response) => {
    try {
        const { ...data } = req.body;
        const password = await bcrypt.hash(data.psw, 5); //HASHEO PSW
        let emailyaexiste = await Persona.find( {email: data.email}); //
        let n = emailyaexiste.length
       
        if(n === 0){
            const persona: IPersona = new Persona({
                nombreCompleto: data.nombreCompleto,
                email: data.email,
                psw: password,
                telefono: data.telefono,
                rol: data.rol,
            });
        await persona.save();
        Logger.info('Metodo CREATE - Status 200');
        Logger.info(persona);
        return res.status(200).json(persona);
        }

        Logger.info('Metodo CREATE - Status 400 - Ya existe el email en la BD');
        return res.status(400).send('Ya existe el email en la BD');

    } catch (error) {
        console.log(error);
        res.status(500).send('Algo salió mal.');
        Logger.info('Metodo CREATE - Status 500 - Algo salió mal.');
    }
};

// Edit a resource
export const update = async (req: Request, res: Response) => {
    const id = req?.params?.id;
    const { ...data } = req.body;
    try {
        let persona = await Persona.findById(id);

        if (!persona){
            Logger.info('Metodo EDIT - Status 404 - '+`No se encontró el producto con id: ${id}`); 
            return res.status(404).send(`No se encontró la persona con id: ${id}`);
        }
                    
        if (data.nombreCompleto) persona.nombreCompleto = data.nombreCompleto;
        //if (data.email) persona.email = data.email; //EDIT no considera la actualización del mismo 
        if (data.psw) persona.psw = data.psw;
        if (data.telefono) persona.telefono = data.telefono;
        if (data.rol) persona.rol = data.rol;
        
        await persona.save();
        res.status(200).json(persona);
        Logger.info('Metodo EDIT - Status 200');
        Logger.info(persona);

    } catch (error) {
        res.status(500).send('Algo salió mal.');
        Logger.info('Metodo EDIT - Status 500 - Algo salió mal.');
    }
};

// Delete a resource
export const destroy = async (req: Request, res: Response) => {
    const id = req?.params?.id;
    try {
        //let persona = await Persona.findByIdAndDelete(id);
        let persona = await Persona.findById(id);
        if (!persona){
            res.status(404).send(`No se encontró la persona con id: ${id}`);
            Logger.info('Metodo DELETE - Status 404 - '+`No se encontró el producto con id: ${id}`); 
        }
            
        else{
            persona.estado = false;
            await persona.save();
             res.status(200).json(persona);
             Logger.info('Metodo DELETE - Status 200');
             Logger.info(persona);
        }
           
    } catch (error) {
        res.status(500).send('Algo salió mal.');
        Logger.info('Metodo DELETE - Status 500 - Algo salió mal.');
    }
};
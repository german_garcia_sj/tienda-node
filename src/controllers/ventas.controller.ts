import { Request, Response } from "express";
import Ventas from "../models/ventas.model";
import IVenta from "../interfaces/ventas.interface";
import Logger from '../utils/loguer'
import app  from '../app';
import bcrypt from 'bcrypt';
import ventasModel from "../models/ventas.model";
import Persona from "../models/persona.model";
import Productos from "../models/productos.model";

// Get all resources
export const index = async (req: Request, res: Response) => {
    // agregar filtros

    try {
        const { ...data } = req.query; 
        let filters = { ...data };//preguntar por esta línea??
        
        if (data.estado) { //preguntar por esta validacion ej (data solo)
            filters = { ...filters, estado: {$regex: data.estado, $options: 'i'} }
        }

        let venta = await Ventas
            .find(filters)
            .populate({ path: 'persona_id', select: ['nombreCompleto', 'email'] })
            .populate({ path: 'productos', select: ['nombre', 'precio', 'estado']});

        res.json(venta);
        Logger.info('Metodo GET ALL - Status 200');
    } catch (error) {
        res.status(500).send('Algo salió mal');
        Logger.info('Metodo GET ALL - Status 500 - Algo salió mal.');
    }
};

// Get one resource
export const show = async (req: Request, res: Response) => {
    const id = req?.params?.id;
    try {
        let venta = await Ventas.findById(id);

        if (!venta){
            res.status(404).send(`No se encontró la venta con id: ${id}`);
            Logger.info('Metodo GET ONE - Status 404 - '+`No se encontró la venta con id: ${id}`);
        }
        else {
            res.json(venta);
            Logger.info('Metodo GET ONE - Status 200');
            Logger.info(venta);
        }
    } catch (error) {
        res.status(500).send('Algo salió mal.');
        Logger.info('Metodo GET ONE - Status 500 - Algo salió mal.');
    }
};

// Create a new resource
export const create = async (req: Request, res: Response) => {
    try {
        const { ...data } = req.body;
         
        const venta: IVenta = new Ventas({
            forma_de_pago: data.forma_de_pago,
            estado: 'Aprobada',
            precio_total:0,
            persona_id: data.persona_id,
        });
            
        await venta.save();
        Logger.info('Metodo CREATE - Status 200');
        Logger.info(venta);
        return res.status(200).json(venta);

    } catch (error) {
        console.log(error);
        res.status(500).send('Algo salió mal.');
        Logger.info('Metodo CREATE - Status 500 - Algo salió mal.');
    }
};

// Edit a resource
export const update = async (req: Request, res: Response) => {
    const id = req?.params?.id;
    const { ...data } = req.body;
    try {
        let venta = await Ventas.findById(id);

        if (!venta){
            Logger.info('Metodo EDIT - Status 404 - '+`No se encontró la venta con id: ${id}`); 
            return res.status(404).send(`No se encontró la venta con id: ${id}`);
        }
                    
        if(data.forma_de_pago) venta.forma_de_pago = data.forma_de_pago;
        if(data.persona_id) venta.persona_id = data.persona_id;

        await venta.save();
        res.status(200).json(venta);
        Logger.info('Metodo EDIT - Status 200');
        Logger.info(venta);

    } catch (error) {
        res.status(500).send('Algo salió mal.');
        Logger.info('Metodo EDIT - Status 500 - Algo salió mal.');
    }
};

// Delete a resource
export const destroy = async (req: Request, res: Response) => {
    const id = req?.params?.id;
    try {
        //let venta = await Ventas.findByIdAndDelete(id); //NO BORRAR REGISTRO - SOLO MARCAR CON PROX LINEA
        let venta = await Ventas.findById(id);
        console.log(venta);
        if (!venta){
            res.status(404).send(`No se encontró la venta con id: ${id}`);
            Logger.info('Metodo DELETE - Status 404 - '+`No se encontró la venta con id: ${id}`); 
        }
            
        else{
             venta.estado='Anulada';
             await venta.save();
             res.status(200).json(venta);
             Logger.info('Metodo DELETE - Status 200');
             Logger.info(venta);
        }
           
    } catch (error) {
        res.status(500).send('Algo salió mal.');
        Logger.info('Metodo DELETE - Status 500 - Algo salió mal.');
    }
};

export const agregarPoducto = async (req: Request, res: Response) => {
    const ventaId = req?.params?.ventaId;
    const productoId = req?.params?.productoId;
    try {
        let producto = await Productos.findById(productoId);
        let venta = await Ventas.findById(ventaId);

        if (!producto || !venta)
            res.status(404).send(`No se encontró la venta o el producto indicado.`);
        else {
            if(producto.stock >0){
                //Actualizar total de la venta
                venta.precio_total = venta.precio_total+producto.precio;        
                //Actualizar Stock
                producto.stock--;    
                // buscar info acerca de transacciones
                await venta?.productos.push(producto?.id);
                await producto?.venta.push(venta?.id);
                await producto?.save();
                await venta?.save();
                return res.status(201).json(venta);
            }
            else{
                return res.status(404).send('No hay stock del producto para agregar a la venta actual');
            }
        }
    } catch (error) {
        res.status(500).send('Algo salió mal.');
    }
};

export const borrarproducto = async (req: Request, res: Response) => {
    const ventaId = req?.params?.ventaId;
    const productoId = req?.params?.productoId;

    try {
        let producto = await Productos.findById(productoId);
        let venta = await Ventas.findById(ventaId);

        if (!producto || !venta)
            res.status(404).send(`No se encontró la venta o el producto indicado.`);
        else {
               // buscar info acerca de transacciones 
                let indiceProducto = Number(venta?.productos.indexOf(productoId));
                let indiceVenta = Number(producto?.venta.indexOf(ventaId)); 

            if (indiceProducto !== -1 && indiceVenta!==-1) {
                //Actualizar total de la venta
                venta.precio_total = venta.precio_total-producto.precio;        
                //Actualizar Stock
                producto.stock++; 
                venta?.productos.splice(indiceProducto,1);
                producto?.venta.splice(indiceVenta,1);   
                await venta?.save();
                await producto?.save();
                return res.status(201).json(venta);  
            }
            else{
                return res.status(404).send('No se encontro el producto a eliminar de la venta actual');
            }
        }
    } catch (error) {
        res.status(500).send('Algo salió mal.');
    }
};